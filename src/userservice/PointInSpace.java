/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userservice;

/**
 *
 * @author rhjvedder
 */
public class PointInSpace {
    /**
     * needs comments as well
     */
    int x;
    int y;
    int z;
    
    /**
     * Calulates euclidian distance to other PointInSpace.
     * @param otherPoint
     * @return distance
     */
    double calculateDistance(PointInSpace otherPoint){
        //calculate euclidian distance
        // used to comment out some code ctrl + /
        double dist = Math.sqrt(
            Math.pow(this.x-otherPoint.x, 2)
                    + Math.pow(this.y-otherPoint.y, 2)
                    + Math.pow(this.z-otherPoint.z, 2)
            );
        return dist;
    }
    
}
